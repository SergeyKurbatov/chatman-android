package com.test.injection.module

import android.app.Application
import android.content.Context
import com.test.injection.qualifier.AppContextQualifier
import com.test.injection.scope.AppScope
import dagger.Module
import dagger.Provides

/**
 * Основной модуль приложения
 * @author StrangeHare
 *         Date: 11.09.17
 */
@Module
@AppScope
class AppModule constructor(private val app: Application) {

    /**
     * Провайдим контекст
     * @return контекст
     */
    @Provides
    @AppScope
    @AppContextQualifier
    fun provideAppContext(): Context = app
}