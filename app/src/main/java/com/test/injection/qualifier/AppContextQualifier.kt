package com.test.injection.qualifier

import javax.inject.Qualifier

/**
 * Квалификатор контекста приложения
 * @author StrangeHare
 *         Date: 11.09.17
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class AppContextQualifier