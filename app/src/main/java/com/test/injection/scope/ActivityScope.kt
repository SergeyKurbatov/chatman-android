package com.test.injection.scope

import javax.inject.Scope

/**
 * Скоуп активити
 * @author StrangeHare
 *         Date: 11.09.17
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope