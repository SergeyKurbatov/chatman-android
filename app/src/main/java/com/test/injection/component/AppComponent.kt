package com.test.injection.component

import com.test.injection.module.AppModule
import com.test.injection.scope.AppScope
import dagger.Component

/**
 * Компонент приложения
 * @author StrangeHare
 *         Date: 11.09.17
 */
@AppScope
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {
}