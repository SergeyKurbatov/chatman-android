package com.test

import android.app.Application
import com.test.injection.component.AppComponent
import com.test.injection.component.DaggerAppComponent

/**
 * Основной объект приложения
 * @author StrangeHare
 *         Date: 11.09.17
 */
class App : Application() {

    /** Статический блок */
    companion object {
        lateinit var instance: App
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        appComponent = DaggerAppComponent.create()
    }

    /** Инстанс приложения */
    fun getInstance(): App = instance

    /** Компонент приложения */
    fun getAppComponent(): AppComponent = appComponent
}