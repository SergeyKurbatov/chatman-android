package com.test.chat.view.entity.holder

import android.support.v4.content.ContextCompat
import android.view.View
import android.view.ViewGroup
import com.test.R
import org.jetbrains.anko.*

/**
 * Вью входящего сообщения
 * @author StrangeHare
 *         Date: 18.09.17
 */
class MessageInHolderView : AnkoComponent<ViewGroup> {

    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
        linearLayout {
            lparams(width = wrapContent, height = wrapContent) {
                leftMargin = dip(16)
                rightMargin = dip(16)
                topMargin = dip(4)
                bottomMargin = dip(4)
            }

            imageView {
                imageResource = R.drawable.ic_account_image
            }.lparams(width = dip(48), height = dip(48)) {
                rightMargin = dip(8)
            }

            relativeLayout {
                lparams(width = matchParent, height = wrapContent)

                textView {
                    id = R.id.message_in_content_text_view
                    background = ContextCompat.getDrawable(context, R.drawable.chat_message_in_bubble)
                    padding = dip(16)
                }.lparams(width = wrapContent, height = wrapContent)

                textView {
                    id = R.id.message_in_date_text_view
                    leftPadding = dip(4)
                }.lparams(width = wrapContent, height = wrapContent) {
                    below(R.id.message_in_content_text_view)
                    alignStart(R.id.message_in_content_text_view)
                }
            }
        }
    }
}