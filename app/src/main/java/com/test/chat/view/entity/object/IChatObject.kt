package com.test.chat.view.entity.`object`

import com.test.chat.view.entity.holder.HolderType

/**
 * Интерфейс объектов чата
 * @author StrangeHare
 *         Date: 18.09.17
 */
interface IChatObject {

    /**
     * Вернуть тип ячейки для чата
     * @return тип ячейки
     */
    val type: HolderType

    /**
     * Получить дату сообщения
     * @return дата
     */
    val date: String
}
