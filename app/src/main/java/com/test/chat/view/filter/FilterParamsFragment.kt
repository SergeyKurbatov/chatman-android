package com.test.chat.view.filter

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.test.R
import com.test.chat.view.chatlist.ChatActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.UI

/**
 * Фрагмент фильтра чата
 * @author StrangeHare
 *         Date: 15.09.17
 */
class FilterParamsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return UI {
            relativeLayout {
                lparams(width = matchParent, height = matchParent)

                linearLayout {
                    lparams(width = matchParent, height = wrapContent)

                    verticalLayout {
                        lparams(width = matchParent, height = wrapContent)

                        radioGroup {
                            radioButton {
                                textResource = R.string.checkbox
                            }.lparams(width = matchParent, height = wrapContent)
                            radioButton {
                                textResource = R.string.checkbox
                            }.lparams(width = matchParent, height = wrapContent)
                        }
                    }.lparams(weight = 5.0f)

                    verticalLayout {
                        lparams(width = matchParent, height = wrapContent)

                        radioGroup {
                            radioButton {
                                textResource = R.string.checkbox
                            }.lparams(width = matchParent, height = wrapContent)
                            radioButton {
                                textResource = R.string.checkbox
                            }.lparams(width = matchParent, height = wrapContent)
                        }
                    }.lparams(weight = 5.0f)
                }

                button(R.string.enter_chat_button_text) {
                    id = R.id.enter_chat_button
                    onClick {
                        startActivity(intentFor<ChatActivity>())
                    }
                }.lparams(width = matchParent, height = wrapContent) {
                    padding = dip(16)
                    alignParentBottom()
                }
            }
        }.view
    }
}