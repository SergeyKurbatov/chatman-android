package com.test.chat.view.entity.holder

import android.support.v4.content.ContextCompat
import android.view.View
import android.view.ViewGroup
import com.test.R
import org.jetbrains.anko.*

/**
 * Вью исходящего сообщения
 * @author StrangeHare
 *         Date: 15.09.17
 */
class MessageOutHolderView : AnkoComponent<ViewGroup?> {

    override fun createView(ui: AnkoContext<ViewGroup?>): View = with(ui) {
        relativeLayout {
            lparams(width = wrapContent, height = wrapContent) {
                leftMargin = dip(16)
                rightMargin = dip(16)
                topMargin = dip(4)
                bottomMargin = dip(4)
            }
            textView {
                id = R.id.message_out_content_text_view
                background = ContextCompat.getDrawable(context, R.drawable.chat_message_out_bubble)
                textColor = ContextCompat.getColor(context, R.color.chat_text_color_white)
                padding = dip(16)
                textSize = 16f
            }.lparams(width = wrapContent, height = wrapContent) {
                alignParentRight()
            }
            textView {
                id = R.id.message_out_date_text_view
                textSize = 12f
                rightPadding = dip(4)
            }.lparams(width = wrapContent, height = wrapContent) {
                below(R.id.message_out_content_text_view)
                alignEnd(R.id.message_out_content_text_view)
            }
        }
    }
}