package com.test.chat.view.entity.`object`

import com.test.chat.view.entity.holder.HolderType


/**
 * Объект сообщения
 * @constructor
 * @param date    дата
 * @param message сообщение
 * @param type    тип
 *
 * @author StrangeHare
 *         Date: 15.09.17
 */
class MessageObject constructor(val message: String,
                                override val date: String,
                                override val type: HolderType) : IChatObject