package com.test.chat.view.entity.holder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.test.R
import com.test.chat.view.entity.`object`.MessageObject

/**
 * Холдер входящего сообщения
 * @constructor
 * @param itemView вью холдера
 *
 * @author StrangeHare
 *         Date: 18.09.17
 */
class MessageInHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    /** Вью сообщ0ения */
    private var messageTextView: TextView = itemView.findViewById(R.id.message_in_content_text_view)
    /** Вью даты */
    private var dateTextView: TextView = itemView.findViewById(R.id.message_in_date_text_view)

    /**
     * Бинд данных
     * @param item сообщение
     */
    fun bind(item: MessageObject) {
        messageTextView.text = item.message
        dateTextView.text = item.date
    }
}