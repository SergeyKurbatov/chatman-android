package com.test.chat.view.entity.holder

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.test.chat.view.entity.`object`.IChatObject
import com.test.chat.view.entity.`object`.MessageObject
import org.jetbrains.anko.AnkoContext

/**
 * Все возможные типы холдеров
 * @author StrangeHare
 *         Date: 18.09.17
 */
enum class HolderType {

    /** Входящие сообщение */
    MESSAGE_IN {
        override fun createViewHolder(parent: ViewGroup?): RecyclerView.ViewHolder =
                MessageInHolder(MessageInHolderView().createView(AnkoContext.create(parent!!.context, parent)))

        override fun bind(holder: RecyclerView.ViewHolder, item: IChatObject) {
            val messageInHolder: MessageInHolder = holder as MessageInHolder
            val messageObject: MessageObject = item as MessageObject
            messageInHolder.bind(messageObject)
        }

        override fun type(): Int = this.javaClass.hashCode()
    },

    /** Исходящее сообщение */
    MESSAGE_OUT {
        override fun createViewHolder(parent: ViewGroup?): RecyclerView.ViewHolder =
                MessageOutHolder(MessageOutHolderView().createView(AnkoContext.create(parent!!.context, parent)))

        override fun bind(holder: RecyclerView.ViewHolder, item: IChatObject) {
            val messageOutHolder: MessageOutHolder = holder as MessageOutHolder
            val messageObject: MessageObject = item as MessageObject
            messageOutHolder.bind(messageObject)
        }

        override fun type(): Int = this.javaClass.hashCode()
    };

    companion object {

        /**
         * Получить тип ячейки по типу вью
         * @param viewType тип вью
         * @return тип ячейки
         */
        fun get(viewType: Int): HolderType {
            HolderType.values()
                    .filter { it.type() == viewType }
                    .forEach { return it }
            throw RuntimeException("Некорректный тип ячейки")
        }
    }

    /**
     * Вернуть тип
     * @return тип макета
     */
    abstract fun type(): Int

    /**
     * Создать холдер ячейки
     * @param parent родитель ячейки
     * @return холдер ячейки
     */
    abstract fun createViewHolder(parent: ViewGroup?): RecyclerView.ViewHolder

    /**
     * Привязать объект к холдеру
     * @param holder холдер
     * @param item   объект
     */
    abstract fun bind(holder: RecyclerView.ViewHolder, item: IChatObject)
}