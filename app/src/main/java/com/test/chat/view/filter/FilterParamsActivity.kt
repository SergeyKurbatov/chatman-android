package com.test.chat.view.filter

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.test.R
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.matchParent

/**
 * Активность задания фильтра чата
 * @author StrangeHare
 *         Date: 15.09.17
 */
class FilterParamsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        frameLayout {
            id = R.id.container
            lparams(width = matchParent, height = matchParent)
            supportFragmentManager.beginTransaction().replace(R.id.container, FilterParamsFragment()).commit()
        }
    }
}
