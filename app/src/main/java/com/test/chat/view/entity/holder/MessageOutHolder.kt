package com.test.chat.view.entity.holder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.test.R
import com.test.chat.view.entity.`object`.MessageObject

/**
 * Холдер исходящего сообщения
 * @constructor
 * @param itemView вью холдера
 *
 * @author StrangeHare
 *         Date: 15.09.17
 */
class MessageOutHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    /** Вью сообщ0ения */
    private var messageTextView: TextView = itemView.findViewById(R.id.message_out_content_text_view)
    /** Вью даты */
    private var dateTextView: TextView = itemView.findViewById(R.id.message_out_date_text_view)

    /**
     * Бинд сообщения
     * @param item исходящее сообщение
     */
    fun bind(item: MessageObject) {
        messageTextView.text = item.message
        dateTextView.text = item.date
    }
}