package com.test.chat.view.entity

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.test.chat.view.entity.`object`.IChatObject
import com.test.chat.view.entity.holder.HolderType

/**
 * Адаптер чата
 * @constructor
 * @param listMessage список сообщений
 *
 * @author StrangeHare
 *         Date: 15.09.17
 */
class ChatAdapter(private var listMessage: List<IChatObject>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder = HolderType.get(viewType).createViewHolder(parent)

    override fun onBindViewHolder(outHolder: RecyclerView.ViewHolder, position: Int) {
        val chatObject: IChatObject = listMessage[position]
        chatObject.type.bind(outHolder, chatObject)
    }

    override fun getItemCount(): Int = listMessage.size

    override fun getItemViewType(position: Int): Int = listMessage[position].type.type()
}