package com.test.chat.view.chatlist

import android.os.Bundle
import android.support.design.R
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.test.chat.presentation.ChatPresenter
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.verticalLayout
import org.jetbrains.anko.wrapContent

/**
 * Фрагмент чата
 * @author StrangeHare
 *         Date: 15.09.17
 */
class ChatFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return UI {
            verticalLayout {
                lparams(width = matchParent, height = matchParent)

                recyclerView {
                    val orientation = LinearLayoutManager.VERTICAL
                    layoutManager = LinearLayoutManager(context, orientation, true)
                    overScrollMode = View.OVER_SCROLL_NEVER
                    adapter = ChatPresenter().adapter
                    R.attr.stackFromEnd
                }.lparams(width = matchParent, height = wrapContent)
            }
        }.view
    }
}