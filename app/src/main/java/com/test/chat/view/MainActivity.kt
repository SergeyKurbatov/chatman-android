package com.test.chat.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.test.chat.view.filter.FilterParamsActivity
import org.jetbrains.anko.intentFor

/**
 * Главная активити
 * @author StrangeHare
 *         Date: 11.09.17
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startActivity(intentFor<FilterParamsActivity>())
    }
}