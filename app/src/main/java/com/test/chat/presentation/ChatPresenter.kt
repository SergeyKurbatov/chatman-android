package com.test.chat.presentation

import com.test.chat.view.entity.`object`.MessageObject
import com.test.chat.view.entity.ChatAdapter
import com.test.chat.view.entity.holder.HolderType

/**
 * Презентер
 * @author StrangeHare
 *         Date: 15.09.17
 */
class ChatPresenter {

    /** Адаптер */
    var adapter: ChatAdapter

    init {
        val list: List<MessageObject> = listOf(MessageObject("dsfdsdsadasdasdsadasdsfs", "10.10.2010", HolderType.MESSAGE_IN), MessageObject("sdfdsfsdsadsadassd", "10.10.2010", HolderType.MESSAGE_IN),
                MessageObject("dssadasdsadasdfdsfs", "10.10.2010", HolderType.MESSAGE_OUT), MessageObject("dsasdasdfdsfs", "10.10.2010", HolderType.MESSAGE_OUT), MessageObject("dsasdsadfdsfs", "10.10.2010", HolderType.MESSAGE_IN),
                MessageObject("dsdsadsfdsfs", "10.10.2010", HolderType.MESSAGE_OUT), MessageObject("dsfdsdadasdsfs", "10.10.2010", HolderType.MESSAGE_OUT), MessageObject("dsfdasdsadasdassfs", "10.10.2010", HolderType.MESSAGE_IN),
                MessageObject("dsfdasdasdasdsfs", "10.10.2010", HolderType.MESSAGE_IN), MessageObject("dsfasdasddsfs", "10.10.2010", HolderType.MESSAGE_OUT), MessageObject("sadsadasdasdsfdsfs", "10.10.2010", HolderType.MESSAGE_IN),
                MessageObject("dsfasdasdsddsfs", "10.10.2010", HolderType.MESSAGE_IN), MessageObject("dsfsadsadasddsfs", "10.10.2010", HolderType.MESSAGE_OUT), MessageObject("dsfdasdasdasdsfs", "10.10.2010", HolderType.MESSAGE_IN),
                MessageObject("dsfsadasdasdadsfs", "10.10.2010",HolderType.MESSAGE_IN), MessageObject("dssadsadasdfdsfs", "10.10.2010", HolderType.MESSAGE_OUT), MessageObject("dsfasdsadasdasdadsfs", "10.10.2010", HolderType.MESSAGE_IN))
        adapter = ChatAdapter(list)
    }
}